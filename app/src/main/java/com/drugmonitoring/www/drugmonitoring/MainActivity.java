package com.drugmonitoring.www.drugmonitoring;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import java.io.Serializable;
import java.sql.Connection;

public class MainActivity extends AppCompatActivity {
    Connection dbconnenction;
    Button btnPrescribe;
    Button btnDispense;
    ProgressBar progressBar;

    // Method Called when the Main Activity if launched
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d("drug","Android Application Started");

        btnPrescribe = findViewById(R.id.prescribe);
        btnDispense = findViewById(R.id.dispense);

        btnPrescribe.setEnabled(true);
        btnDispense.setEnabled(true);

        progressBar = findViewById(R.id.progressbar);


        progressBar.setVisibility(View.INVISIBLE);
        btnPrescribe.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(view.getContext(), PrescriptionActivity.class);
            intent.putExtra("db", (Serializable) dbconnenction);
            view.getContext().startActivity(intent);
            btnPrescribe.setEnabled(false);
            btnDispense.setEnabled(false);
            progressBar.setVisibility(View.VISIBLE);


        }

        });
        btnDispense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), DispensationActivity.class);
                view.getContext().startActivity(intent);
                btnPrescribe.setEnabled(false);
                btnDispense.setEnabled(false);
                progressBar.setVisibility(View.VISIBLE);
            }

        });


    }

    // Method Called when the MainActivity is resumed
    @Override
    protected void onResume() {
        super.onResume();
        btnPrescribe.setEnabled(true);
        btnDispense.setEnabled(true);
        progressBar.setVisibility(View.INVISIBLE);

    }
}
