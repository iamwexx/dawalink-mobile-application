package com.drugmonitoring.www.drugmonitoring;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class PrescriptionActivity extends AppCompatActivity {
    Spinner doctorslistSpinner;
    Spinner druglistSpinner;
    EditText dosage;
    EditText duration;
    EditText direction;
    EditText patientName;
    Connection dbconnenction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescription);

        doctorslistSpinner = findViewById(R.id.doc_spinner);
        List<String> doctorIDlist = new ArrayList();
        List<String> doctorlist = new ArrayList();
        druglistSpinner = findViewById(R.id.drug_spinner);
        List<String> drugIDlist = new ArrayList();
        List<String> druglist = new ArrayList();

        dosage = findViewById(R.id.drug_dosage);
        duration = findViewById(R.id.drug_duration);
        direction = findViewById(R.id.drug_directions);
        patientName = findViewById(R.id.patient_name);

        Button btnSubmit = findViewById(R.id.submitForm);

//        druglistSpinner.getSelectedItemId();
        Log.d("drug","Prescribe Activity Launched...");

        // As of Version 9, UI threads are no longer allowed to connect to the Internet unless explicitly allowed.
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitNetwork().build();
            StrictMode.setThreadPolicy(policy);

        try {
            PostgreSQLconnector connector = new PostgreSQLconnector();
            dbconnenction = connector.makeConnection();

            Statement statement = dbconnenction.createStatement();
            Log.d("drug","Reading doctors table...");

            ResultSet docresultSet = statement.executeQuery("SELECT licence_no, name FROM tb_doctors;");

            while (docresultSet.next()) {
                drugIDlist.add(docresultSet.getString("licence_no"));
                doctorlist.add(docresultSet.getString("name"));

            }

//            Log.d("drug",docresultSet.toString());

            ResultSet drugresultSet = statement.executeQuery("SELECT drug_id, name_of_product FROM tb_drugs;");

            while (drugresultSet.next()) {
                doctorIDlist.add(drugresultSet.getString("drug_id"));
                druglist.add(drugresultSet.getString("name_of_product"));
            }

        } catch (SQLException e) {
            Log.d("drug",e.toString());

            e.printStackTrace();
        }


// Create the list view and bind the adapter

        ArrayAdapter<String> doctorAdapter = new ArrayAdapter(this,
                android.R.layout.simple_spinner_item, doctorlist);
        doctorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        doctorslistSpinner.setAdapter(doctorAdapter);

        ArrayAdapter<String> drugAdapter = new ArrayAdapter(this,
                android.R.layout.simple_spinner_item, druglist);
        drugAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        druglistSpinner.setAdapter(drugAdapter);
//
//        btnSubmit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uniqueId = numbGen();
                String selectedDoc = doctorslistSpinner.getSelectedItem().toString();
                String selectedDrug = druglistSpinner.getSelectedItem().toString();
                LocalDateTime issuedate = LocalDateTime.now();
                String selectedDosage = dosage.getText().toString();
                String selectedDuration = duration.getText().toString();
                String selectedDirection = direction.getText().toString();
                String selectedPationetName = patientName.getText().toString();


                Log.d("drug","Inserting Data to Database .. ");
            try {
                Statement statement = dbconnenction.createStatement();

                int docresultSet = statement.executeUpdate("INSERT INTO tb_prescription (prescription_id, doctor_id, drug_id, issue_date, dose_regimen, duration_of_treatment,patient_name,direction_of_treatment) VALUES ('"+uniqueId+"', '"+selectedDoc+"', '"+selectedDrug+"' ,'"+issuedate+"', '"+selectedDosage+"', '"+selectedDuration+"', '"+selectedPationetName+"','"+selectedDirection+"');");

                if(docresultSet == 1){
                    Intent intent = new Intent(view.getContext(), PrescriptionBarCode.class);
                    intent.putExtra("prescriptionId", uniqueId);

                    view.getContext().startActivity(intent);
                }


            } catch (SQLException e) {
                Log.d("drug",e.toString());

                e.printStackTrace();
            }

            }

        });
    }

    public static String numbGen() {
        while (true) {
            long numb = (long)(Math.random() * 100000000 * 1000000); // had to use this as int's are to small for a 13 digit number.
            if (String.valueOf(numb).length() == 13)
                return String.valueOf(numb);
        }
    }
}
