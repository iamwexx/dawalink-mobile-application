package com.drugmonitoring.www.drugmonitoring;

import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;



public class PostgreSQLconnector {

    public Connection makeConnection() {
        Log.d("drug","Trying to connect to RDS database..");

        /* Declare and initialize a sql Connection variable. */
        Connection connection = null;
        try {
            /* Create connection url. */
            String mysqlConnUrl = "jdbc:postgresql://drugmonitoring.chgbwn1bsz5m.us-east-2.rds.amazonaws.com:5432/drugmonitoring";

            /* user name. */
            String mysqlUserName = "jill";

            /* password. */
            String mysqlPassword = "YYyy^^66";

            /* Get the Connection object. */
            connection = DriverManager.getConnection(mysqlConnUrl, mysqlUserName , mysqlPassword);


            Log.d("drug","Successfully! Connected to PostgreSQL database!");
            return connection;
        } catch (Exception e) {
            Log.d("drug","Error found tryong to connect to RDS database");

            System.out.println(e);
            Log.d("drug","ERROR: "+e.toString());

            return null;

        }

    }
}

