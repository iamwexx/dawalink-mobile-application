package com.drugmonitoring.www.drugmonitoring;

        import android.content.Intent;
        import android.graphics.Bitmap;
        import android.graphics.Canvas;
        import android.graphics.Color;
        import android.graphics.RectF;
        import android.graphics.drawable.Drawable;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ImageView;
        import android.widget.TextView;

        import com.google.zxing.BarcodeFormat;
        import com.google.zxing.MultiFormatWriter;
        import com.google.zxing.WriterException;
        import com.google.zxing.common.BitMatrix;
        import com.journeyapps.barcodescanner.BarcodeEncoder;
        import com.onbarcode.barcode.android.AndroidColor;
        import com.onbarcode.barcode.android.IBarcode;
        import com.onbarcode.barcode.android.QRCode;

        import java.sql.Connection;
        import java.sql.ResultSet;
        import java.sql.SQLException;
        import java.sql.Statement;

public class PrescriptionBarCode extends AppCompatActivity {

    Connection dbconnenction;
    String prescritionID,doctorId,drugId,doseregimen,durationofTreat,issueDate,patienName, direction;

    String licenceNo,phoneContact,hospitalName;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescriptionbarcode);
        ImageView bbbbb = findViewById(R.id.barCodeView);
        // Get the transferred data from source activity.
        Intent intent = getIntent();
        String prescriptionId = intent.getStringExtra("prescriptionId");
//        TextView textView = findViewById(R.id.testtext);
        try {
            PostgreSQLconnector connector = new PostgreSQLconnector();
            dbconnenction = connector.makeConnection();
// Retrieve prescriptoio
            Statement statementPres = dbconnenction.createStatement();
        ResultSet preresultSet = statementPres.executeQuery("SELECT * FROM tb_prescription WHERE prescription_id='"+prescriptionId+"';");

        while (preresultSet.next()) {
            prescritionID=preresultSet.getString("prescription_id");
            doctorId=preresultSet.getString("doctor_id");
            drugId=preresultSet.getString("drug_id");
            doseregimen=preresultSet.getString("dose_regimen");
            durationofTreat=preresultSet.getString("duration_of_treatment");
            issueDate=preresultSet.getString("issue_date");
            patienName=preresultSet.getString("patient_name");
            direction=preresultSet.getString("direction_of_treatment");
//                    getString("licence_no");


        }
        //retrieve doctor info
            Statement statementDoc = dbconnenction.createStatement();
            ResultSet docresultSet = statementDoc.executeQuery("SELECT * FROM tb_doctors WHERE name='"+doctorId+"';");

            while (docresultSet.next()) {
                licenceNo=docresultSet.getString("licence_no");
                phoneContact=docresultSet.getString("phone_number");
                hospitalName=docresultSet.getString("hospital_name");

            }

//            textView.setText(phoneContact);
        } catch (SQLException e) {
            Log.d("drug",e.toString());

            e.printStackTrace();
        }


//        BarCodeGenerator view = new BarCodeGenerator(this);
//        view.getParent();
//        bbbbb.setV
//        setContentView(view);
        String text = String.join(",", prescritionID,doctorId,drugId,doseregimen,durationofTreat,issueDate,patienName, direction,licenceNo,phoneContact,hospitalName);
       // Whatever you need to encode in the QR code
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE,900,900);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            bbbbb.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }


    }
}



